import sqlite3
from sqlite3 import Error
from datetime import date
from eveApi import Esi


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
    return None


class Types:
    def __init__(self):
        self.connection = create_connection("cache.db")
        self.cursor = self.connection.cursor()
        self.esi = Esi()

    def __del__(self):
        self.connection.close()

    def get_name(self, type_id):
        try:
            self.cursor.execute("SELECT name FROM types WHERE id=?", (type_id,))
            result = self.cursor.fetchall()
            if len(result) == 0:
                type_name = self.esi.get_type_name(type_id)
                self.cursor.execute("INSERT INTO types VALUES(?, ?)", (type_id, type_name))
                self.connection.commit()
                return type_name
            else:
                return result[0][0]
        except Error as e:
            print(e)

    def get_price(self, type_id):
        try:
            self.cursor.execute("SELECT price, modified FROM prices WHERE id=?", (type_id,))
            result = self.cursor.fetchall()
            cur_date = date.today().isoformat()
            if len(result) == 0 or result[0][1] != cur_date:
                price = self.esi.get_price(type_id)
                if len(result) == 0:
                    self.cursor.execute("INSERT INTO prices VALUES(?, ?, ?)", (type_id, price, cur_date))
                else:
                    if result[0][0] > 0 and price > 0:
                        price = (result[0][0] + price) / 2
                    elif result[0][0] > 0:
                        price = result[0][0]
                    self.cursor.execute("UPDATE prices SET price=?, modified=? WHERE id=?", (price, cur_date, type_id))
                self.connection.commit()
                return price
            else:
                return result[0][0]
        except Error as e:
            print(e)

    def get_insurance(self, ship_id):
        try:
            self.cursor.execute("SELECT insurance, modified FROM insurances WHERE id=?", (ship_id,))
            result = self.cursor.fetchall()
            cur_date = date.today().isoformat()
            if len(result) == 0 or result[0][1] != cur_date:
                insurance = self.esi.get_insurance(ship_id)
                if len(result) == 0:
                    self.cursor.execute("INSERT INTO insurances VALUES(?, ?, ?)", (ship_id, insurance, cur_date))
                else:
                    self.cursor.execute("UPDATE insurances SET insurance=?, modified=? WHERE id=?", (insurance, cur_date, ship_id))
                self.connection.commit()
                return insurance
            else:
                return result[0][0]
        except Error as e:
            print(e)
