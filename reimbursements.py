from eveTypes import Types
from eveApi import get_json, Esi
from srpList import SrpList
from http.server import BaseHTTPRequestHandler, HTTPServer
import cgi
try:  # py3
    from urllib.parse import urlparse, parse_qsl
except ImportError:  # py2
    from urlparse import urlparse, parse_qsl


class Server(BaseHTTPRequestHandler):
    def write(self, text):
        self.wfile.write(bytes(text, "utf-8"))

    def set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_HEAD(self):
        self.set_headers()

    def get_link(self, ispost=False):
        killmail = None
        if ispost:
            form = cgi.FieldStorage(fp=self.rfile, headers=self.headers, environ={'REQUEST_METHOD': 'POST'})
            if form["link"].value:
                killmail = form["link"].value
        else:
            qs = dict(parse_qsl(urlparse(self.path).query))
            if qs.get("id") and qs.get("hash"):
                killmail = "https://esi.evetech.net/latest/killmails/{}/{}/".format(qs.get("id"), qs.get("hash"))
        return killmail

    def write_form(self):
        self.write("<html><head><title>Ship Replacement Program</title></head>")
        self.write("<body><p><img src='https://i.imgur.com/VoBkA79.png'></img>")
        self.write("<form action='' method='POST'>ESI Link: <input type='text' name='link' size='75'>")
        self.write("<input type='submit' value='Calculate'></form></p>")
        self.write("</body></html>")

    def write_result(self, killmail):
        self.write("<html><head><title>Ship Replacement Program</title></head><body><p>")
        self.calculate(killmail)
        self.write("</p></body></html>")

    def build_page(self, ispost=False):
        self.set_headers()
        killmail = self.get_link(ispost)
        if killmail:
            self.write_result(killmail)
        else:
            self.write_form()

    def do_GET(self):
        self.build_page()

    def do_POST(self):
        self.build_page(True)

    def calculate(self, killmail):
        try:
            srp_list = SrpList()
            types = Types()
            data = get_json(killmail)
            ship_id = data["victim"]["ship_type_id"]
            ship_name = types.get_name(ship_id)
            self.write("Reimbursable ship: {}</br>".format(ship_name))
            if srp_list.ship_exists(ship_name):
                ship_price = types.get_price(ship_id)
                self.write("Ship price: {:,.2f} ISK</br>".format(ship_price))
                ship_insurance = types.get_insurance(ship_id)
                self.write("Ship insurance: {:,.2f} ISK</br>".format(ship_insurance))
                srp_percent = srp_list.percent(ship_name) / 100
                self.write("Reimbursement percent: {:.0f}</br>".format(srp_percent * 100))
                fit_price = 0
                self.write("Ship fit:</br>")
                for item in data["victim"]["items"]:
                    if item["flag"] < 11 or item["flag"] > 143:  # skip not fitted item
                        continue
                    item_id = item["item_type_id"]
                    item_name = types.get_name(item_id)
                    if srp_list.module_exists(ship_name, item_name):
                        if "quantity_destroyed" in item:
                            item_count = item["quantity_destroyed"]
                        elif "quantity_dropped" in item:
                            item_count = item["quantity_dropped"]
                        else:
                            item_count = 0
                        item_price = types.get_price(item_id) * item_count
                        self.write("{} x{}: {:,.2f} ISK</br>".format(item_name, item_count, item_price))
                        fit_price += item_price
                self.write("Total fit price: {:,.2f} ISK</br>".format(fit_price))

                character_name = Esi().get_character_name(data["victim"]["character_id"])
                self.write("</br><font color='red' size='5'>PILOT NAME: {}</font>".format(character_name))

                total_srp = round(((ship_price + fit_price - ship_insurance) * srp_percent) / 1000000)
                self.write("</br><font color='red' size='5'>REIMBURSEMENT VALUE: %d000000 ISK</font>" % total_srp)

                comment = killmail.split("/")[5]
                self.write("</br><font color='red' size='5'>COMMENT: https://zkillboard.com/kill/{}/</font>".format(comment))
            else:
                self.write("<font color='red' size='5'>Ship not in SRP list</font>")
        except:
            self.write("<font color='red' size='5'>Error while calculating reimbursement value</font>")


if __name__ == "__main__":
    server_address = ('127.0.0.1', 8080)
    httpd = HTTPServer(server_address, Server)
    httpd.serve_forever()
